<?php

namespace Placeto\FreshsalesSdk\Carrier;

use GuzzleHttp\Exception\BadResponseException;
use Placeto\FreshsalesSdk\Contracts\FreshsalesOperations;
use Placeto\FreshsalesSdk\Exceptions\FreshsalesServiceException;
use Placeto\FreshsalesSdk\Helpers\ArrayHelper;
use Placeto\FreshsalesSdk\Requests\BulkDeletesLeadsRequest;
use Placeto\FreshsalesSdk\Requests\LeadRequest;
use Placeto\FreshsalesSdk\Responses\DeletResponse;
use Placeto\FreshsalesSdk\Responses\Response;
use Throwable;

class Request extends FreshsalesOperations
{
    private const ENDPOINT_BASE = 'api/leads';
    private const ENDPOINT_FORGET = '/forget';
    private const ENDPOINT_BULK_DELETE_LEADS = '/bulk_destroy';
    private const ENDPOINT_CLONE = '/clone';
    private const ENDPOINT_CONVERT = '/convert';
    private const ENDPOINT_LIST_ALL_LEAD_FIELDS = 'api/settings/leads/fields';
    private const ENDPOINT_LIST_ALL_LEADS = '/view';
    private const ENDPOINT_LIST_ALL_ACTIVITIES = '/activities';

    private function makeRequest(string $url, string $action, array $arguments = [])
    {
        try {
            $data = ArrayHelper::filter($arguments);

            $response = $this->settings->getClient()->$action($url, [
                'json' => $data,
                'headers' => $this->settings->getHeaders(),
            ]);

            $result = $response->getBody()->getContents();
        } catch (BadResponseException $e) {
            $result = $e->getResponse()->getBody()->getContents();
        } catch (Throwable $e) {
            throw FreshsalesServiceException::fromServiceException($e);
        }

        return json_decode($result, true);
    }

    public function createLead(LeadRequest $createLeadRequest): Response
    {
        $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_BASE), 'post', $createLeadRequest->toArray());

        return new Response($result);
    }

    public function viewLead(array $data): Response
    {
        if (isset($data['include'])) {
            $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_BASE . '/' . $data['id'], $data['include']), 'get');
        } else {
            $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_BASE . '/' . $data['id']), 'get');
        }

        return new Response($result);
    }

    public function deleteLead(array $data): DeletResponse
    {
        $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_BASE . '/' . $data['id']), 'delete');

        return new DeletResponse($result);
    }

    public function forgetLead(array $data): DeletResponse
    {
        $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_BASE . '/' . $data['id'] . self::ENDPOINT_FORGET), 'delete');

        return new DeletResponse($result);
    }

    public function bulkDeletesLeads(BulkDeletesLeadsRequest $bulkDeletesLeadsRequest): Response
    {
        $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_BASE . self::ENDPOINT_BULK_DELETE_LEADS), 'post', $bulkDeletesLeadsRequest->toArray());

        return new Response($result);
    }

    public function cloneLead(array $data): Response
    {
        $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_BASE . '/' . $data['id'] . self::ENDPOINT_CLONE), 'post');

        return new Response($result);
    }

    public function updateLead(LeadRequest $updateLeadRequest): Response
    {
        $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_BASE . '/' . $updateLeadRequest->getId()), 'put', $updateLeadRequest->toArray());

        return new Response($result);
    }

    public function convertLead(LeadRequest $convertLeadRequest): Response
    {
        $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_BASE . '/' . $convertLeadRequest->getId() . self::ENDPOINT_CONVERT), 'post', $convertLeadRequest->toArray());

        return new Response($result);
    }

    public function listAllLeadFields(array $data): Response
    {
        if (isset($data['include'])) {
            $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_LIST_ALL_LEAD_FIELDS, $data['include']), 'get');
        } else {
            $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_LIST_ALL_LEAD_FIELDS), 'get');
        }

        return new Response($result);
    }

    public function listAllLeads(int $viewId): Response
    {
        $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_BASE . self::ENDPOINT_LIST_ALL_LEADS . '/' . $viewId), 'get');

        return new Response($result);
    }

    public function listAllActivities(int $id): Response
    {
        $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_BASE . '/' . $id . self::ENDPOINT_LIST_ALL_ACTIVITIES), 'get');

        return new Response($result);
    }
}
