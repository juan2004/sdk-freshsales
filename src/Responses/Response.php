<?php

namespace Placeto\FreshsalesSdk\Responses;

use Placeto\FreshsalesSdk\Contracts\Entity;

class Response extends Entity
{
    protected array $response = [];

    public function __construct($data)
    {
        $this->response = $data;
    }

    public function getResponse(): array
    {
        return $this->response;
    }

    public function toArray(): array
    {
        return $this->arrayFilter([
            'response' => $this->response,
        ]);
    }
}
