<?php

namespace Placeto\FreshsalesSdk\Contracts;

use Placeto\FreshsalesSdk\Helpers\ArrayHelper;
use Placeto\FreshsalesSdk\Traits\LoaderTrait;

abstract class Entity
{
    use LoaderTrait;

    abstract public function toArray(): array;

    protected function arrayFilter(array $array): array
    {
        return ArrayHelper::filter($array);
    }
}
