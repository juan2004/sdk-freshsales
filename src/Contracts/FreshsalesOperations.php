<?php

namespace Placeto\FreshsalesSdk\Contracts;

use Placeto\FreshsalesSdk\Helpers\Settings;
use Placeto\FreshsalesSdk\Requests\BulkDeletesLeadsRequest;
use Placeto\FreshsalesSdk\Requests\LeadRequest;
use Placeto\FreshsalesSdk\Responses\DeletResponse;
use Placeto\FreshsalesSdk\Responses\Response;

abstract class FreshsalesOperations
{
    protected Settings $settings;

    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    abstract public function createLead(LeadRequest $createLeadRequest): Response;

    abstract public function viewLead(array $data): Response;

    abstract public function deleteLead(array $data): DeletResponse;

    abstract public function forgetLead(array $data): DeletResponse;

    abstract public function bulkDeletesLeads(BulkDeletesLeadsRequest $bulkDeletesLeadsRequest): Response;

    abstract public function cloneLead(array $data): Response;

    abstract public function updateLead(LeadRequest $updateLeadRequest): Response;

    abstract public function convertLead(LeadRequest $convertLeadRequest): Response;

    abstract public function listAllLeadFields(array $data): Response;

    abstract public function listAllLeads(int $viewId): Response;

    abstract public function listAllActivities(int $id): Response;
}
