<?php

namespace Placeto\FreshsalesSdk\Requests;

use Placeto\FreshsalesSdk\Contracts\Entity;

class BulkDeletesLeadsRequest extends Entity
{
    protected array $selected_ids;

    public function __construct($data)
    {
        $this->load($data, ['selected_ids']);
    }

    public function getSelectedIds(): array
    {
        return $this->selected_ids;
    }

    public function toArray(): array
    {
        return $this->arrayFilter([
            'selected_ids' => $this->getSelectedIds(),
        ]);
    }
}
