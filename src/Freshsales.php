<?php

namespace Placeto\FreshsalesSdk;

use Placeto\FreshsalesSdk\Exceptions\FreshsalesException;
use Placeto\FreshsalesSdk\Helpers\Settings;
use Placeto\FreshsalesSdk\Requests\BulkDeletesLeadsRequest;
use Placeto\FreshsalesSdk\Requests\LeadRequest;
use Placeto\FreshsalesSdk\Responses\DeletResponse;
use Placeto\FreshsalesSdk\Responses\Response;

class Freshsales
{
    private const MESSAGE = 'Wrong class request';

    protected Settings $settings;

    public function __construct(array $data)
    {
        $this->settings = new Settings($data);
    }

    public function createLead($dataRequest): Response
    {
        if (is_array($dataRequest)) {
            $createLeadRequest = new LeadRequest($dataRequest);
        }

        if (!($createLeadRequest instanceof LeadRequest)) {
            throw FreshsalesException::forDataNotProvided(self::MESSAGE);
        }

        return $this->settings->getFreshsalesOperations()->createLead($createLeadRequest);
    }

    public function viewLead($dataRequest): Response
    {
        return $this->settings->getFreshsalesOperations()->viewLead($dataRequest);
    }

    public function deleteLead($dataRequest): DeletResponse
    {
        return $this->settings->getFreshsalesOperations()->deleteLead($dataRequest);
    }

    public function forgetLead($dataRequest): DeletResponse
    {
        return $this->settings->getFreshsalesOperations()->forgetLead($dataRequest);
    }

    public function bulkDeleteLeads($dataRequest): Response
    {
        if (is_array($dataRequest)) {
            $bulkDeletesLeadRequest = new BulkDeletesLeadsRequest($dataRequest);
        }

        if (!($bulkDeletesLeadRequest instanceof BulkDeletesLeadsRequest)) {
            throw FreshsalesException::forDataNotProvided(self::MESSAGE);
        }

        return $this->settings->getFreshsalesOperations()->bulkDeletesLeads($bulkDeletesLeadRequest);
    }

    public function cloneLead($dataRequest): Response
    {
        return $this->settings->getFreshsalesOperations()->cloneLead($dataRequest);
    }

    public function updateLead($dataRequest): Response
    {
        if (is_array($dataRequest)) {
            $updateLeadRequest = new LeadRequest($dataRequest);
        }

        if (!($updateLeadRequest instanceof LeadRequest)) {
            throw FreshsalesException::forDataNotProvided(self::MESSAGE);
        }

        return $this->settings->getFreshsalesOperations()->updateLead($updateLeadRequest);
    }

    public function convertLead($dataRequest): Response
    {
        if (is_array($dataRequest)) {
            $convertLeadRequest = new LeadRequest($dataRequest);
        }

        if (!($convertLeadRequest instanceof LeadRequest)) {
            throw FreshsalesException::forDataNotProvided(self::MESSAGE);
        }

        return $this->settings->getFreshsalesOperations()->convertLead($convertLeadRequest);
    }

    public function listAllLeadFields(array $dataRequest): Response
    {
        return $this->settings->getFreshsalesOperations()->listAllLeadFields($dataRequest);
    }

    public function listAllLeads(int $viewId): Response
    {
        return $this->settings->getFreshsalesOperations()->listAllLeads($viewId);
    }

    public function listAllActivities(int $id): Response
    {
        return $this->settings->getFreshsalesOperations()->listAllActivities($id);
    }
}
