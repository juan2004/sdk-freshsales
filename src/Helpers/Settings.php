<?php

namespace Placeto\FreshsalesSdk\Helpers;

use GuzzleHttp\Client;
use Placeto\FreshsalesSdk\Carrier\Request;
use Placeto\FreshsalesSdk\Contracts\Entity;
use Placeto\FreshsalesSdk\Contracts\FreshsalesOperations;
use Placeto\FreshsalesSdk\Exceptions\FreshsalesException;

class Settings extends Entity
{
    private const MESSAGE1 = 'No token provided';
    private const MESSAGE2 = 'No service URL provided to use';

    protected string $token;
    public string $baseUrl = '';
    protected array $headers = [];

    protected int $timeout = 15;
    protected bool $verifySsl = true;

    protected ?Client $client = null;

    protected ?FreshsalesOperations $freshsalesOperations = null;

    public function __construct(array $data)
    {
        if (!isset($data['token'])) {
            throw FreshsalesException::forDataNotProvided(self::MESSAGE1);
        }

        if (!isset($data['baseUrl']) || !filter_var($data['baseUrl'], FILTER_VALIDATE_URL)) {
            throw FreshsalesException::forDataNotProvided(self::MESSAGE2);
        }

        if (substr($data['baseUrl'], -1) != '/') {
            $data['baseUrl'] .= '/';
        }

        $allowedKeys = [
            'token',
            'baseUrl',
            'headers',
        ];

        $this->load($data, $allowedKeys);
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function baseUrl(string $endpoint = '', string $include = ''): string
    {
        if (isset($include)) {
            return $this->baseUrl . $endpoint . '?include=' . $include;
        }
        return $this->baseUrl . $endpoint;
    }

    public function getHeaders(): array
    {
        $this->headers = [
            'Authorization' => 'Token token=' . $this->getToken(),
        ];
        return $this->headers;
    }

    public function getTimeout(): int
    {
        return $this->timeout;
    }

    public function isVerifySsl(): bool
    {
        return $this->verifySsl;
    }

    public function getClient(): Client
    {
        if (!$this->client) {
            $this->client = new Client([
                'timeout' => $this->getTimeout(),
                'connect_timeout' => $this->getTimeout(),
                'verify' => $this->isVerifySsl(),
            ]);
        }

        return $this->client;
    }

    public function getFreshsalesOperations(): FreshsalesOperations
    {
        if ($this->freshsalesOperations instanceof FreshsalesOperations) {
            return $this->freshsalesOperations;
        } else {
            $this->freshsalesOperations = new Request($this);
        }

        return $this->freshsalesOperations;
    }

    public function toArray(): array
    {
        return [];
    }
}
