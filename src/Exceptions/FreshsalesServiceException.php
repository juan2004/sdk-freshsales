<?php

namespace Placeto\FreshsalesSdk\Exceptions;

use Throwable;

class FreshsalesServiceException extends FreshsalesException
{
    private const MESSAGE = 'Error handling operation';

    public static function fromServiceException(Throwable $e): self
    {
        return new self(self::MESSAGE, 100, $e);
    }
}
