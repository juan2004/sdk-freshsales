<?php

namespace Placeto\FreshsalesSdk\Exceptions;

use Exception;

class FreshsalesException extends Exception
{
    public static function forDataNotProvided(string $message = ''): self
    {
        return new self($message);
    }
}
