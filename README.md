# Freshsales Sdk

Software development kit to connect with the Freshsale's APIs

## Installation
You should add PlacetoPay repository:
```json
{
    "repositories": [
        {
            "type": "composer",
            "url": "https://dev.placetopay.com/repository"
        }
    ]
}
```

Then, you can install the package via composer:
```
composer require placeto/freshsales-sdk
```

## Usage

For more information about the Freshsale's APIs see the documentation [Here](https://developer.freshsales.io/api/#introduction)